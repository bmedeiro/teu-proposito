package com.example.lifepath;

public class DataDAO {
    public String getSobre() {
        return sobre;
    }

    String sobre = "na numerologia, o seu número de propósito de vida revela o caminho encarregado de te estimular e engrandecer. " +
            "\n" +
            "\n" +
            "além disso, seu número tem como objetivo mostrar as lições que estão no seu caminho e " +
            "as características pessoais que lhe ajudam a conqusitar o seu propósito." +
            "\n" +
            "\n" +
            "a descoberta do seu número de propósito de vida é feita através da soma dos algarismos de sua data de nascimento. " +
            "\n" +
            "\n" +
            "o resultado obtido representa o significado de sua existência, mostrando sua missão nesta vida.";

    String id1 = "O INDEPENDENTE";
    String number1 = "NÚMERO UM";
    String numero1 = "o seu propósito envolve trabalhar sua segurança, para que você consiga ter iniciativa e " +
            "determinação, assumir riscos, o controle e evitar o conforto." +
            "\n" +
            "\n" +
            "é vital que você desenvolva confiança em si" +
            " mesmo para transmitir essa característica aos outros."+
            "\n" +
            "\n" +
            "você é uma pessoa que gosta de resolver os problemas. muitas vezes, prefere resolvê-los sem a ajuda de ninguém." +
            "é uma pessoa dinâmica, inovadora e impulsiva."+
            "\n" +
            "\n" +
            "se você acreditar em suas próprias habilidades, terá boas chances de se tornar um grande líder."+
            "\n" +
            "\n" +
            "além disso, é importante que você evite o orgulho ou o autoritarismo, é preciso fazer com que as" +
            " pessoas confiem em você através do respeito oriundo de sua liderança, não do medo.";
    String id2 = "O EMOTIVO";
    String number2 = "NÚMERO DOIS";
    String numero2 = "o seu propósito é aprender a ser receptivo e disponível."+
             "\n" +
             "\n" +
            "é necessário agir com calma, buscando a paz e evitando conflitos.  trabalhar sua confiança é uma tarefa muito importante, pois " +
            "sua timidez e sensibilidade pode transmitir  uma mensagem de insegurança."+
            "\n" +
            "\n" +
            "Entretanto, focar na sua confiança fará com que você encontre seu equilíbrio emocional." +
            "\n" +
            "\n" +
            "você é uma pessoa que sempre busca ajudar o próximo. por ter uma sensibilidade pura, é capa de ouvir e " +
            "compreender as pessoas ao seu redor." +
            "\n" +
            "\n" +
            "além disso, você é uma pessoa emotiva e apreciadora de bons sentimentos. para alcançar seu propósito de " +
            "vida você deve nutrir-se de bons sentimentos e partilhá-los com todos que te rodeiam.";

    String id3 = "O CRIATIVO";
    String number3 = "NÚMERO TRÊS";
    String numero3 = "o seu propósito de vida é trabalhar o dom da comunicação."+
            "\n" +
            "\n" +
            "você gosta de relecionar-se com as pessoas ao seu redor e de expressar seus sentimentos. " +
            "portanto, é muito importante entrar em contato com suas próprias emoções, para que você consiga entender " +
            "o que deve comunicar aos outros." +
            "\n" +
            "\n" +
            "você é sociável e admirador da arte. possui mente aberta e considera muito importante apreciar a beleza. " +
            "\n" +
            "\n" +
            "você também é impulsivo e ambicioso – é importante que você reflita antes de tomar decisões importantes, " +
            "evitando ser superficial e canalizando sua energia para que seus projetos venham a ter sucesso.";

    String id4 = "O DISCIPLINADO";
    String number4 = "NÚMERO QUATRO";
    String numero4 = "seu propósito de vida exige perseverança, estabilidade e organização para desenvolver alicerces firmes " +
            "para você e o ambiente no qual você vive." +
            "\n" +
            "\n" +
            "você é uma pessoa leal e honesta ao seus princípios de vida. é uma pessoa vista como reservada. " +
            "\n" +
            "\n" +
            "entretanto, adora estar entre amigos e gosta de conhecer pessoas e lugares. além disso, você gosta de estudar e " +
            "de debates sobre variados assuntos."+
            "\n" +
            "\n" +
            "você é uma pessoa organizada e disciplinada. por gostar de estabilidade e organização, tende a economizar dinheiro " +
            "e a possuir bens materiais. pode vir a se sentir frustrada com mudanças, mas é necessário que você trabalhe sua " +
            "flexibilidade."+
            "\n" +
            "\n" +
            "Você deve tentar compreender diferentes pontos de vidas e, também, adaptar-se a mudanças, caso elas" +
            "ocorram.";

    String id5 = "O AVENTUREIRO";
    String number5 = "NÚMERO CINCO";
    String numero5 = "você é uma pessoa que gosta de aventura e de quebrar rotinas. " +
            "adora viajar, desbravar situações ainda desconhecidas e viver no presente." +
            "\n" +
            "\n" +
            " é uma pessoa energética e pode, às vezes, tornar-se impaciente." +
            "\n" +
            "\n" +
            "Lembre-se que é importante sempre compreender e respeitar a vontade do outro para que todos possam " +
            "crescer em harmonia."+
            "\n" +
            "\n" +
            "por ser uma pessoa que adora conhecer novos lugares, pessoas e cotidianos, seu propósito de vida envolve trabalhar o seu compromisso pessoal com os outros." +
            "\n" +
            "\n" +
            "além diso, é importante que você veja cada situação em sua vida como um oportunidade de aprender algo novo, trabalhando a sua paciência e, também, o seu respeito aos demais.";

    String id6 = "O HARMÔNICO";
    String number6 = "NÚMERO SEIS";
    String numero6 = "você é uma pessoa de bom coração, uma pessoa pacífica e conciliadora, que gosta de unir todos ao seu redor, evitando conflitos e solidão. " +
            "\n" +
            "\n" +
            "você gosta de demasiadamente de cuidar dos outros e muitas vezes, por querer proteger os demais, você acaba se esquecendo de cuidar de você mesmo." +
            "\n" +
            "\n" +
            "portanto, seu propósito de vida envolve trabalhar os seus sentimentos interiores." +
            "\n" +
            "\n" +
            "é necessário que você cuide bem de você mesmo, assim você conseguirá criar um ambiente de harmonia para todos ao seu redor. ";

    String id7 = "O IDEALISTA";
    String number7 = "NÚMERO SETE";
    String numero7 = "seu propósito de vida está relacionado ao crescimento interior. " +
            "\n" +
            "\n" +
            "você é uma pessoa observadora e analítica, algumas pessoas podem te ver como uma pessoa misteriosa e introspectiva." +
            "\n" +
            "\n" +
            "entretanto, você é extremamente amigável  e transmite calma e paz para as pessoas ao seu redor"+
            "\n" +
            "\n" +
            "além  disso, você é uma pessoa questionadora e adora buscar o significado por trás de todos os porquês, por gostar de conhecimento e ter o hábito de ler e refletir, você é muito independente. " +
            "\n" +
            "\n" +
            "entretanto, você tende a ser reservado(a), desconfiado(a) e, até mesmo, solitário(a)." +
            "\n" +
            "\n" +
            "a reflexão e o estudo interno são muito importantes para o seu crescimento, mas lembre-se de que os eventos externos e a conexão com diferentes pessoas devem, também, te trazer aprendizados. ";


    String id8 = "O AMBICIOSO";
    String number8 = "NÚMERO OITO";
    String numero8 = "você é uma pessoa organizada e prática, gosta de almejar o sucesso e mostra seu caráter " +
            "e seu senso de justiça em suas tarefas diárias." +
            "\n" +
            "\n" +
            "você tem o espírito empreendedor, é uma pessoa ambiciosa que deseja ter uma vida material estável. " +
            "\n" +
            "\n" +
            "seu propósito de vida está relacionado ao equilíbrio da vida espiritual e a material. é necessário que você compreenda " +
            "melhor as pessoas ao seu redor, valoriza sua caminhada interior e sua humildade." +
            "\n" +
            "\n" +
            "o sucesso pessoal e profissional está no seu caminho e, com coragem e humildade você conquistará tudo que sonha.";

    String id9 = "O HUMANISTA";
    String number9 = "NÚMERO NOVE";
    String numero9 = "você é um altruísta."+
            "\n" +
            "\n" +
            "você possui a capacidade de compreender as pessoas ao seu redor, proporcionando-os sabedoria e amor." +
            "além disso, você é uma pessoa artística e adora ajudar os demais." +
            "\n" +
            "\n" +
            "é uma pessoa huminatária: sente-se bem ajudando os próximos e busca alcançar seus ideais de maneira compartilhada, criando um mundo pacífico ao seu redor." +
            "\n" +
            "\n" +
            "seu propósito de vida envolve doar atenção e amor sem esperar nada em troca. seu sucesso pessoal virá devagar, de pouco em pouco, quando você não estiver esperando-o." +
            "\n" +
            "\n" +
            "lembre-se que compreender os demais não é uma tarefa fácil, mas é necessária para seu crescimento pessoal.";

    String id11 = "O SONHADOR";
    String number11 = "NÚMERO ONZE";
    String numero11 = "seu propósito de vida envolve exercer um papel importante na sociedade onde está inserido(a)."+
            "\n" +
            "\n" +
            "você precisa trabalhar sua evolução espiritual diante dos problemas que aparece em sua vida."+
            "\n" +
            "\n" +
            "você é uma pessoa sonhadora, idealista. seu propósito de vida envolve cooperar com os demais para qeu todos consigam alcançar seus respectivos sonhos."+
            "\n" +
            "\n" +
            "lembre-se de usar sua intuição sempre que sentir necessário. o seu eu interior te guiará de maneira sábia.";
    String id22 = "O CRIADOR";
    String number22 = "NÚMERO VINTE E DOIS";
    String numero22 = "seu propósito de vida envolve a ajudar a Humanidade."+
            "\n" +
            "\n" +
            "você tem uma energia de mudança. É uma pessoa que identifica o que está errado e move para resolver o problema."+
            "\n" +
            "\n" +
            "você tem uma criatividade única e precisa focar sua energia em ações positivas para que você e o mundo ao seu redor possam se beneficiar disso."+
            "\n" +
            "\n" +
            "o número de propósito de 22 é considerado um número especial. pessoas com esse número possui uma espiritualidade elevada e deve focar, sempre, em realizar o bem para o universo.";

    String id33 = "O DOADOR";
    String number33 = "NÚMERO TRINTA E TRÊS ";
    String numero33 = "seu propósito de vida envolve doar-se para atingir a harmônia pura."+
            "\n" +
            "\n" +
            "é necessário ter corgem e amor incondicional para se alinhar ao seu propósito. você deve procurar trabalhar" +
            "com as pessoas ao seu redor, ajudando-as a alcançar seus objetios para que assim o seu seja alcançado."+
            "\n" +
            "\n" +
            "você é uma  pessoa que emana humildade e coragem e seguindo esses princípios você irá inspirar a humanidade"+

            "\n" +
            "\n" +
            "lembre-se de sempre entregar-se plenamente a atitudes de doação e aos bons sentimentos.";



    public String getId1() {
        return id1;
    }

    public String getNumber1() {
        return number1;
    }

    public String getNumero1() {
        return numero1;
    }

    public String getId2() {
        return id2;
    }

    public String getNumber2() {
        return number2;
    }

    public String getNumero2() {
        return numero2;
    }

    public String getId3() {
        return id3;
    }

    public String getNumber3() {
        return number3;
    }

    public String getNumero3() {
        return numero3;
    }

    public String getId4() {
        return id4;
    }

    public String getNumber4() {
        return number4;
    }

    public String getNumero4() {
        return numero4;
    }

    public String getId5() {
        return id5;
    }

    public String getNumber5() {
        return number5;
    }

    public String getNumero5() {
        return numero5;
    }

    public String getId6() {
        return id6;
    }

    public String getNumber6() {
        return number6;
    }

    public String getNumero6() {
        return numero6;
    }

    public String getId7() {
        return id7;
    }

    public String getNumber7() {
        return number7;
    }

    public String getNumero7() {
        return numero7;
    }

    public String getId8() {
        return id8;
    }

    public String getNumber8() {
        return number8;
    }

    public String getNumero8() {
        return numero8;
    }

    public String getId9() {
        return id9;
    }

    public String getNumber9() {
        return number9;
    }

    public String getNumero9() {
        return numero9;
    }

    public String getId11() {
        return id11;
    }

    public String getNumber11() {
        return number11;
    }

    public String getNumero11() {
        return numero11;
    }

    public String getId22() {
        return id22;
    }

    public String getNumber22() {
        return number22;
    }

    public String getNumero22() {
        return numero22;
    }

    public String getId33() {
        return id33;
    }

    public String getNumber33() {
        return number33;
    }

    public String getNumero33() {
        return numero33;
    }
}

