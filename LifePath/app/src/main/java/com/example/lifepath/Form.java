package com.example.lifepath;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;
import java.util.Timer;
import java.util.TimerTask;

public class Form extends AppCompatActivity {
TextView logo, title, title2, day, month, year;
EditText day1, day2;
EditText month1, month2;
EditText year1, year2, year3, year4;
int number;
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        getSupportActionBar().hide();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        makeBarTransparent();

        Typeface font = Typeface.createFromAsset( getResources().getAssets() , "fontes/AristoCaps.ttf");
        Typeface font1 = Typeface.createFromAsset( getResources().getAssets() , "fontes/AristoBold.ttf");

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.form);
        FlowingGradientClass grad = new FlowingGradientClass();
        grad.setBackgroundResource(R.drawable.transition)
                .onRelativeLayout(rl)
                .start();


        day1 = (EditText) findViewById(R.id.editDay1);
        day2 = (EditText) findViewById(R.id.editDay2);


        month1 = (EditText) findViewById(R.id.editMonth1);
        month2 = (EditText) findViewById(R.id.editMonth2);


        year1 = (EditText) findViewById(R.id.editYear1);
        year2 = (EditText) findViewById(R.id.editYear2);
        year3 = (EditText) findViewById(R.id.editYear3);
        year4 = (EditText) findViewById(R.id.editYear4);

        logo = (TextView) findViewById(R.id.logoForm);
        logo.setText("               TEU" + "\n" + "PROPÓSITO");
        logo.setTextSize(16);
        logo.setTypeface(font1);

        title = (TextView) findViewById(R.id.title);
        title.setText("QUAL A SUA");
        title.setTextSize(32);
        title.setTypeface(font);

        title2 = (TextView) findViewById(R.id.title2);
        title2.setText("DATA DE NASCIMENTO?");
        title2.setTextSize(24);
        title2.setTypeface(font);


        Button dualBtn = findViewById(R.id.dualBtn);
        dualBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Timer().schedule(new TimerTask(){
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void run() {

                        if (testing()) {
                            calculate();

                            Intent i = new Intent(Form.this, Result.class);
                            i.putExtra("number", number);
                            startActivity(i);
                        }else{
                            Form.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Houve algum problema no cálculo do seu número ):" +  "\n" + "\n" +"Insira novamente sua data de nascimento", Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                    }
                }, 1000);
            }
        });

        loadDay(font);
        loadMonth(font);
        loadYear(font);
        setupUI(rl);
    }

    @SuppressLint("ResourceAsColor")
    public void loadDay(Typeface font){

        //day1


        day1.setTextSize(24);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            day1.setTextAlignment(EditText.TEXT_ALIGNMENT_CENTER);
        }
        day1.setInputType(InputType.TYPE_CLASS_NUMBER);
        day1.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(1) });
        day1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                if(day1.getText().toString().length()==1)     //size as per your requirement
                {
                    day2.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        day1.requestFocus();

        //day2
        day2.setTextSize(24);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            day2.setTextAlignment(EditText.TEXT_ALIGNMENT_CENTER);
        }
        day2.setInputType(InputType.TYPE_CLASS_NUMBER);
        day2.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(1) });
        day2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                if(day2.getText().toString().length()==1)     //size as per your requirement
                {
                    month1.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });


        day = (TextView) findViewById(R.id.dia);
        day.setTextSize(20);
        day.setTypeface(font);

    }

    @SuppressLint("ResourceAsColor")
    public void loadMonth(Typeface font){
        //month1


        month1.setTextSize(24);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            month1.setTextAlignment(EditText.TEXT_ALIGNMENT_CENTER);
        }
        month1.setInputType(InputType.TYPE_CLASS_NUMBER);
        month1.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(1) });
        month1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                if(month1.getText().toString().length()==1)     //size as per your requirement
                {
                    month2.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        //month2
        month2.setTextSize(24);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            month2.setTextAlignment(EditText.TEXT_ALIGNMENT_CENTER);
        }
        month2.setInputType(InputType.TYPE_CLASS_NUMBER);
        month2.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(1) });
        month2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                if(month2.getText().toString().length()==1)     //size as per your requirement
                {
                    year1.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        month = (TextView) findViewById(R.id.month);
        month.setTextSize(20);
        month.setTypeface(font);

    }

    @SuppressLint("ResourceAsColor")
    public void loadYear(Typeface font){
        //year1

        year1.setTextSize(24);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            year1.setTextAlignment(EditText.TEXT_ALIGNMENT_CENTER);
        }
        year1.setInputType(InputType.TYPE_CLASS_NUMBER);
        year1.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(1) });
        year1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                if(year1.getText().toString().length()==1)     //size as per your requirement
                {
                    year2.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        //year2


        year2.setTextSize(24);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        year2.setTextAlignment(EditText.TEXT_ALIGNMENT_CENTER);
    }
        year2.setInputType(InputType.TYPE_CLASS_NUMBER);
        year2.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(1) });
        year2.addTextChangedListener(new TextWatcher() {

        public void onTextChanged(CharSequence s, int start,int before, int count)
        {
            if(year2.getText().toString().length()==1)     //size as per your requirement
            {
                year3.requestFocus();
            }
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub

        }
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
        }

    });

        //year3

        year3.setTextSize(24);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            year3.setTextAlignment(EditText.TEXT_ALIGNMENT_CENTER);
        }
        year3.setInputType(InputType.TYPE_CLASS_NUMBER);
        year3.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(1) });
        year3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                if(year3.getText().toString().length()==1)     //size as per your requirement
                {
                    year4.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        //year4
        year4.setTextSize(24);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            year4.setTextAlignment(EditText.TEXT_ALIGNMENT_CENTER);
        }
        year4.setInputType(InputType.TYPE_CLASS_NUMBER);
        year4.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(1) });
        year4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                if(year4.getText().toString().length()==1)     //size as per your requirement
                {
                    closeKeyboard(year4);

                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        year = (TextView) findViewById(R.id.ano);
        year.setTextSize(20);
        year.setTypeface(font);

    }


    public boolean testing(){
        int number;
        int numberDay1;
        int numberDay2;
        int numberMonth1;
        int numberMonth2;
        int numberYear1;
        int numberYear2;
        int numberYear3;
        int numberYear4;

        if(day1.getText().toString().equals("") || day2.getText().toString().equals("")){
            numberDay1 = 0;
            numberDay2 = 0;
        }else{
            numberDay1 = Integer.parseInt(day1.getText().toString());
            numberDay2 = Integer.parseInt(day2.getText().toString());
        }
        if(month1.getText().toString().equals("") || month2.getText().toString().equals("")){
            numberMonth1 = 0;
            numberMonth2 = 0;
        }else{

            numberMonth1 = Integer.parseInt(month1.getText().toString());
            numberMonth2 = Integer.parseInt(month2.getText().toString());

        }
        if (year1.getText().toString().equals("") || year2.getText().toString().equals("") || year3.getText().toString().equals("") || year4.getText().toString().equals("")){
            numberYear1 = 0;
            numberYear2 = 0;
            numberYear3 = 0;
            numberYear4 = 0;
        } else {
             numberYear1 = Integer.parseInt(year1.getText().toString());
             numberYear2 = Integer.parseInt(year2.getText().toString());
             numberYear3 = Integer.parseInt(year3.getText().toString());
             numberYear4 = Integer.parseInt(year4.getText().toString());

        }
        number = numberDay1 + numberDay2 + numberMonth1 + numberMonth2 + numberYear1 + numberYear2 + numberYear3 + numberYear4;
        String temp = String.valueOf(number);
        if(temp.length()<=1) {
          return true;
        }

        return true;
    }
    public void calculate() {
        int numberDay1 = Integer.parseInt(day1.getText().toString());
        int numberDay2 = Integer.parseInt(day2.getText().toString());

        int numberMonth1 = Integer.parseInt(month1.getText().toString());
        int numberMonth2 = Integer.parseInt(month2.getText().toString());

        int numberYear1 = Integer.parseInt(year1.getText().toString());
        int numberYear2 = Integer.parseInt(year2.getText().toString());
        int numberYear3 = Integer.parseInt(year3.getText().toString());
        int numberYear4 = Integer.parseInt(year4.getText().toString());

        int temp_number = numberDay1 + numberDay2 + numberMonth1 + numberMonth2 + numberYear1 + numberYear2 + numberYear3 + numberYear4;

        if (temp_number == 11 || temp_number == 22 || temp_number == 33) {
            number = temp_number;

        }
        if (temp_number != 11 && temp_number != 22 && temp_number != 33) {
            String temp = String.valueOf(temp_number);


            if (temp.length() > 1) {
                char um = temp.charAt(0);
                char dois = temp.charAt(1);
                String digit1 = String.valueOf(um);
                String digit2 = String.valueOf(dois);
                temp_number = Integer.valueOf(digit1) + Integer.valueOf(digit2);
                number = temp_number;


                String temp2 = String.valueOf(temp_number);

                if (temp2.length() > 1) {
                    char um2 = temp2.charAt(0);
                    char dois2 = temp2.charAt(1);
                    String digit1_2 = String.valueOf(um2);
                    String digit2_2 = String.valueOf(dois2);
                    temp_number = Integer.valueOf(digit1_2) + Integer.valueOf(digit2_2);
                    number = temp_number;

                }

                String temp3 = String.valueOf(temp_number);

                if (temp3.length() > 1) {
                    char um3 = temp3.charAt(0);
                    char dois3 = temp3.charAt(1);
                    String digit1_3 = String.valueOf(um3);
                    String digit2_3 = String.valueOf(dois3);
                    temp_number = Integer.valueOf(digit1_3) + Integer.valueOf(digit2_3);
                    number = temp_number;

                }

                String temp4 = String.valueOf(temp_number);

                if (temp4.length() > 1) {
                    char um4 = temp4.charAt(0);
                    char dois4 = temp4.charAt(1);
                    String digit1_4 = String.valueOf(um4);
                    String digit2_4 = String.valueOf(dois4);
                    temp_number = Integer.valueOf(digit1_4) + Integer.valueOf(digit2_4);
                    number = temp_number;
                }


            }else{
                    number = temp_number;


            }

        }
    }




    private void closeKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public void makeBarTransparent(){
        //make translucent statusBar on kitkat devices
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(Form.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


}



