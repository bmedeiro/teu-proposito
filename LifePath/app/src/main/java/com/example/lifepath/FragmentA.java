package com.example.lifepath;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;

import org.w3c.dom.Text;

import me.biubiubiu.justifytext.library.JustifyTextView;

@SuppressLint("ValidFragment")
class FragmentA extends Fragment {
    RelativeLayout rl;
    FlowingGradientClass grad;
    Typeface font1;
    TextView logo, titleA, titleA2;
    JustifyTextView textoResultado;
    int number = 0;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        font1 = Typeface.createFromAsset( getResources().getAssets() , "fontes/AristoCaps.ttf");
        grad = new FlowingGradientClass();

        if(null != getArguments()){
            number = getArguments().getInt("number");
        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_a, container, false);
        DataDAO d = new DataDAO();


        switch (number){
            case 11:
              loadResult(view, d.getId11(), d.getNumber11(),d.getNumero11());
                break;
            case 22:
                loadResult(view, d.getId22(), d.getNumber22(),d.getNumero22());
                break;
            case 33:
                loadResult(view, d.getId33(), d.getNumber33(),d.getNumero33());
                break;
            case 1:
                loadResult(view, d.getId1(), d.getNumber1(),d.getNumero1());
                break;
            case 2:
                loadResult(view, d.getId2(), d.getNumber2(),d.getNumero2());
                break;
            case 3:
                loadResult(view, d.getId3(), d.getNumber3(),d.getNumero3());
                break;
            case 4:
                loadResult(view, d.getId4(), d.getNumber4(),d.getNumero4());
                break;
            case 5:
                loadResult(view, d.getId5(), d.getNumber5(),d.getNumero5());
                break;
            case 6:
                loadResult(view, d.getId6(), d.getNumber6(),d.getNumero6());
                break;
            case 7:
                loadResult(view, d.getId7(), d.getNumber7(),d.getNumero7());
                break;
            case 8:
                loadResult(view, d.getId8(), d.getNumber8(),d.getNumero8());
                break;
            case 9:
                loadResult(view, d.getId9(), d.getNumber9(),d.getNumero9());
                break;


        }





        return view;

    }

    public void loadResult(View view, String id, String number, String result){
        logo = (TextView)  view.findViewById(R.id.logoFormFragmentA);
        logo.setText("               TEU" + "\n" + "PROPÓSITO");
        logo.setTextSize(16);
        logo.setTypeface(font1);

        titleA = (TextView)  view.findViewById(R.id.titleA);
        titleA.setText(id);
        titleA.setTextSize(32);
        titleA.setTypeface(font1);

        titleA2 = (TextView)  view.findViewById(R.id.titleA2);
        titleA2.setText(number);
        titleA2.setTextSize(24);
        titleA2.setTypeface(font1);

        textoResultado = (JustifyTextView)  view.findViewById(R.id.textoResultado);
        textoResultado.setText(result);
        textoResultado.setTextSize(20);
        textoResultado.setTypeface(font1);

    }


}