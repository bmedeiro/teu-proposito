package com.example.lifepath;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;

import java.io.DataOutput;

import me.biubiubiu.justifytext.library.JustifyTextView;

@SuppressLint("ValidFragment")
class FragmentB extends Fragment {

    Typeface font1, font2;
    TextView logo, titleB, titleB2;
    JustifyTextView sobre;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        font1 = Typeface.createFromAsset( getResources().getAssets() , "fontes/AristoCaps.ttf");
        font2 = Typeface.createFromAsset( getResources().getAssets() , "fontes/Bellovia.ttf");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        DataDAO d = new DataDAO();
        View view = inflater.inflate(R.layout.fragment_b, container, false);

        logo = (TextView)  view.findViewById(R.id.logoFormFragmentB);
        logo.setText("               TEU" + "\n" + "PROPÓSITO");
        logo.setTextSize(16);
        logo.setTypeface(font1);

        titleB = (TextView)  view.findViewById(R.id.titleB);
        titleB.setText("SOBRE");
        titleB.setTextSize(32);
        titleB.setTypeface(font1);

        titleB2 = (TextView)  view.findViewById(R.id.titleB2);
        titleB2.setText("NUMEROLOGIA");
        titleB2.setTextSize(24);
        titleB2.setTypeface(font1);

        sobre = (JustifyTextView)  view.findViewById(R.id.sobre);
        sobre.setText(d.getSobre());
        sobre.setTextSize(20);
        sobre.setTypeface(font1);



        return view;
    }
}