package com.example.lifepath;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.ColorRes;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;
import com.ss.bottomnavigation.BottomNavigation;
import com.ss.bottomnavigation.events.OnSelectedItemChangeListener;

import java.io.ByteArrayOutputStream;
import java.io.File;

import static android.graphics.Color.TRANSPARENT;
import static com.example.lifepath.Form.setWindowFlag;


public class Result extends AppCompatActivity {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    int number;
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        if(null != getIntent()){
           number = getIntent().getIntExtra("number", -1);
        }


        RelativeLayout rl = (RelativeLayout) findViewById(R.id.result);
        FlowingGradientClass grad = new FlowingGradientClass();
        grad.setBackgroundResource(R.drawable.transition)
                .onRelativeLayout(rl)
                .start();


        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //Mostrar o botão
        getSupportActionBar().setHomeButtonEnabled(true);      //Ativar o botão
        getSupportActionBar().setTitle("");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        makeBarTransparent();

        BottomNavigation bottomNavigation=(BottomNavigation)findViewById(R.id.bottom_navigation);
        bottomNavigation.setDefaultItem(1);
        bottomNavigation.setOnSelectedItemChangeListener(new OnSelectedItemChangeListener() {
            @Override
            public void onSelectedItemChanged(int itemId) {
                switch (itemId){

                    case R.id.tab_about:
                        transaction=getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.result,new FragmentB());
                        break;

                    case R.id.tab_result:
                        Bundle bundle = new Bundle();
                        bundle.putInt("number", number);
                        transaction=getSupportFragmentManager().beginTransaction();
                        FragmentA fA = new FragmentA();
                        transaction.replace(R.id.result, fA);
                        fA.setArguments(bundle);
                        break;

                    case R.id.tab_share:
                        transaction=getSupportFragmentManager().beginTransaction();

                        switch (number){
                            case 11:
                                shareImage(R.drawable.onze);
                                break;
                            case 22:
                                shareImage(R.drawable.vinteedois);
                                break;
                            case 33:
                                shareImage(R.drawable.trintaetres);
                                break;
                            case 1:
                                shareImage(R.drawable.um);
                                break;
                            case 2:
                                shareImage(R.drawable.dois);
                                break;
                            case 3:
                                shareImage(R.drawable.tres);
                                break;
                            case 4:
                                shareImage(R.drawable.quatro);
                                break;
                            case 5:
                                shareImage(R.drawable.cinco);
                                break;
                            case 6:
                                shareImage(R.drawable.seis);
                                break;
                            case 7:
                                shareImage(R.drawable.sete);
                                break;
                            case 8:
                                shareImage(R.drawable.oito);
                                break;
                            case 9:
                                shareImage(R.drawable.nove);
                                break;


                        }


                        break;
                }
                transaction.commit();
            }
        });

    }
    public void makeBarTransparent(){
        //make translucent statusBar on kitkat devices
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(TRANSPARENT);
        }
    }

    private static final int SOLICITAR_PERMISSAO = 1;
    private void shareImage(int imagem) {

        int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, SOLICITAR_PERMISSAO);
        } else {
            Bitmap b = BitmapFactory.decodeResource(getResources(), imagem);
            Intent share = new Intent(Intent.ACTION_SEND);

            share.setType("image/jpeg");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), b, "Número de propósito de vida", null);
            Uri imageUri =  Uri.parse(path);
            share.putExtra(Intent.EXTRA_STREAM, imageUri);
            share.putExtra(Intent.EXTRA_SUBJECT, "Teu Propósito");
            share.putExtra(Intent.EXTRA_TEXT, "O meu número de propósito de vida é o "+number+"." + "\n" + "Conheça o seu também! Baixe o app Teu Propósito :-)");


            startActivity(Intent.createChooser(share, "Selecione"));

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed(){
        startActivity(new Intent(this, Form.class));
        finishAffinity();
        return;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Form.class));
                finishAffinity();
                break;
            default:break;
        }
        return true;
    }
}
