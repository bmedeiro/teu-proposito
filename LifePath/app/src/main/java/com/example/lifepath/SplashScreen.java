package com.example.lifepath;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;

public class SplashScreen extends AppCompatActivity implements Runnable {
     TextView logo;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        handler.postDelayed(this,2500); // utilização do handler para deixar o logo aparecendo por 2 segundos

        Typeface font = Typeface.createFromAsset( getResources().getAssets() , "fontes/AristoBold.ttf");

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.splash);
        FlowingGradientClass grad = new FlowingGradientClass();
        grad.setBackgroundResource(R.drawable.transition)
                .onRelativeLayout(rl)
                .setTransitionDuration(4000)
                .start();

        logo = (TextView) findViewById(R.id.logo);
        logo.setText("TEU" + "\n" + "PROPÓSITO");
        logo.setTextSize(48);
        logo.setTypeface(font);

}

    @Override
    public void run() {
        startActivity(new Intent(this, Form.class));
        finish();
    }
}
